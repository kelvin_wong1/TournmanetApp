package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/gorilla/mux"
	"github.com/gorilla/schema"
	"github.com/gorilla/securecookie"
	"github.com/gorilla/sessions"
	"github.com/kwong21/ryuji-ng/cache"
	"github.com/kwong21/ryuji-ng/models"
	"github.com/urfave/negroni"
)

type Env struct {
	db         models.DataStore
	store      *sessions.CookieStore
	cacheStore cache.CacheStore
}

var decode *schema.Decoder
var env *Env

func main() {
	db, err := models.NewDB(config.GetDatabaseConnectionString())
	cache, err := cache.NewStore(config.GetRedisConnectionString(), 0)
	store := sessions.NewCookieStore(securecookie.GenerateRandomKey(16))

	store.Options = &sessions.Options{
		Path:     "/",
		MaxAge:   config.Application.MaxAge,
		HttpOnly: true,
	}

	if err != nil {
		fmt.Printf("Cannot start application - Database connection error: %v", err)
		return
	}

	env = &Env{db: db, store: store, cacheStore: cache}

	f, err := os.OpenFile("logs/ryuji.log", os.O_APPEND|os.O_CREATE|os.O_RDWR, 0666)

	if err != nil {
		fmt.Printf("error opening log file: %v", err)
	}

	defer f.Close()
	log.SetOutput(f)

	n := negroni.New()
	n.Use(negroni.NewLogger())

	createNewDecoder()

	server := &http.Server{
		Addr:           config.Application.Address,
		Handler:        n,
		ReadTimeout:    time.Duration(config.Application.ReadTimeout * int64(time.Second)),
		WriteTimeout:   time.Duration(config.Application.WriteTimeout * int64(time.Second)),
		MaxHeaderBytes: 1 << 20,
	}

	/* err = server.ListenAndServeTLS("/etc/letsencrypt/live/allvsall.com/fullchain.pem", "/etc/letsencrypt/live/allvsall.com/privkey.pem")
	if err != nil {
		log.WithFields(log.Fields{
			"Log":   "Init",
			"Error": err,
		}).Fatal("Could not start app!")
	} */

	n.UseHandler(router())
	log.Fatal(server.ListenAndServe())
}

func router() *mux.Router {
	rtr := mux.NewRouter()
	files := http.FileServer(http.Dir("includes"))
	rtr.PathPrefix("/includes/").Handler(http.StripPrefix("/includes/", files))

	// public pages
	rtr.Handle("/", http.HandlerFunc(routeHomePage))
	rtr.Handle("/logout", http.HandlerFunc(routeUserLogout)).Methods("GET")
	rtr.Handle("/{page}", http.HandlerFunc(routePublicPages)).Methods("GET")

	rtr.Handle("/sign-up", http.HandlerFunc(routeUserSignUp)).Methods("POST")
	rtr.Handle("/login", http.HandlerFunc(routeUserLogin)).Methods("POST")

	rtr.Handle("/user/overview", checkForAuthenticatedSession(http.HandlerFunc(routeUserOverview)))

	return rtr
}
