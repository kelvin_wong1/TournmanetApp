package cache

import (
	"encoding/json"
	"fmt"
	"github.com/google/go-cmp/cmp"
	"github.com/kwong21/ryuji-ng/models"
	"testing"
)

var token = "3fa6f344-07d1-41bc-90b4-4d2642f06d25"
var mockCache = MockCache{}

func TestCache_StoreSession(t *testing.T) {
	a := constructValidStructForTesting()
	serializedAccount, err := json.Marshal(a)

	if err != nil {
		t.Errorf("Expecting no errors when serializing account but got %v", err)
	}

	mockCache.StoreSession(token, serializedAccount)

	if len(cacheMap) != 1 {
		t.Errorf("Expecting one account in the mock cacheMap but got %v", len(cacheMap))
	}

}
func TestCache_RetrieveProfile(t *testing.T) {
	a := mockCache.RetrieveProfile(token)
	valid := constructValidStructForTesting()

	// password []byte should differ every time
	// setting them to be the same

	valid.Password = a.Password

	if !cmp.Equal(a, valid) {
		t.Errorf("Expecting created Account struct to match known valid Account struct\n")
		fmt.Printf("Retrieved Account struct from cache: %v\n", a)
		fmt.Printf("Valid created Account struct %v\n", valid)
	}

	if !cmp.Equal(a.Profile, valid.Profile) {
		t.Errorf("Expecting created Profile struct to match known valid Profile struct\n")
		fmt.Printf("Retrieved Profile struct from cache: %v\n", a)
		fmt.Printf("Valid created Profile struct %v\n", valid)
	}
}

func constructValidStructForTesting() *models.Account {
	u := new(models.Account)
	p := new(models.Profile)

	p.Team = "Budo MMA"
	p.Instructor = "Scott"
	p.GivenName = "Roger"
	p.Surname = "Rabbit"
	p.DateOfBirth = "1986-12-01"

	u.Profile = p
	u.Email = "roger.rabbit@gmail.com"
	u.SetHashedPassword("password")

	return u
}

