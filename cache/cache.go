package cache

import (
	"fmt"
	"github.com/go-redis/redis"
	"github.com/kwong21/ryuji-ng/models"
	log "github.com/sirupsen/logrus"
	"time"
)

type CacheStore interface {
	StoreSession(token string, serializedStuct []byte) error
	RetrieveProfile(token string) *models.Account
}

type Cache struct {
	*redis.Client
}

type MockCache struct{}

func NewStore(redisConnection string, dbIndex int) (*Cache, error) {

	client := redis.NewClient(&redis.Options{
		Addr:     redisConnection,
		Password: "", // no password set
		DB:       0,  // use default DB
	})

	_, err := client.Ping().Result()

	return &Cache{client}, err

}

func (cacheStore *Cache) StoreSession(token string, serializedStuct []byte) error {

	err := cacheStore.HSet("login", token, serializedStuct).Err()

	err = cacheStore.ZAdd("recent", redis.Z{Score: float64(time.Now().Unix()), Member: token}).Err()

	if err != nil {
		log.WithFields(log.Fields{
			"Log":   "cache sessions",
			"Error": err,
		}).Error("Error creating session for authentication in cache")
	} else {
		log.WithFields(log.Fields{
			"Log": "sessions",
		}).Info(fmt.Sprintf("Created cache with token %v", token))
	}

	return err
}

func (cacheStore *Cache) RetrieveProfile(toke string) *models.Account {
	return nil
}
