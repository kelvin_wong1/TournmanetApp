$( function() {
    $( "#DateOfBirthSelector" ).calendar(
        {
            type: "date",
            monthFirst: true,
            formatter: {
              date: function (date, settings) {
                if (!date) return '';
                var day = date.getDate();
                var month = date.getMonth() + 1 ;
                var year = date.getFullYear();
                return year + '-' + ('0' + month).slice(-2) + '-' + ('0' + day).slice(-2) ;
              }
            }
        }
    );
  } );

$('.ui.form')
    .form({
        fields: {
            fiirstName: {
                identifier: 'GivenName',
                rules: [
                    {
                        type: 'empty',
                        prompt: 'Please provide your first name.'
                    }
                ]
            },
            lastName: {
                identifier: 'Surname',
                rules: [
                    {
                        type: 'empty',
                        prompt: 'Please provide your last name'
                    }
                ]
            },
            email: {
                identifier: 'Email',
                rules: [
                    {
                        type: 'empty',
                        prompt: 'Please provide an email address'
                    },
                    {
                        type: 'email',
                        prompt: 'Please provide a valid email address'
                    }
                ]
            },
            emailConfirm: {
                identifier: 'EmailConfirm',
                rules: [
                    {
                        type: 'empty',
                        prompt: 'Please re-enter your email address'
                    },
                    {
                        type: 'match[Email]',
                        prompt: 'The two email addresses must match'
                    }
                ]
            },
            Password: {
                identifier: 'Password',
                rules: [
                    {
                        type: 'empty',
                        prompt: 'Please enter a password'
                    },
                    {
                        type: 'minLength[6]',
                        prompt: 'Your password must be at least {ruleValue} characters'
                    }
                ]
            },
            PasswordConfirm: {
                identifier: 'PasswordConfirm',
                rules: [
                    {
                        type: 'empty',
                        prompt: 'Please re-enter your passwords'
                    },
                    {
                        type: 'match[Password]',
                        prompt: 'The two passwords must match'
                    }
                ]
            }
        }
    })
    ;