
$(document).ready(
    function() {
        if ($('.slider-images').length > 0 ){
            initSlick();
        }

        writeCurrentYear();

        $('.ui.dropdown').dropdown();
    }
);

/**
* Init for Slick slider
*/
function initSlick(){
    $('.slider-images').slick({
        autoplay: true,
        dots: true,
    });
};

/**
 * Set current year for the footer
 */
function writeCurrentYear(){
    var date = new Date();
    var year = date.getFullYear();
    var copyright = year + ' All vs. All Fight Tournaments';
    $('.copy-year').append(copyright);
}