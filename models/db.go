package models

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	log "github.com/sirupsen/logrus"
)

type DataStore interface {
	CreateAccount(a *Account) (error, int64)
	CreateProfile(p *Profile, accountID int64) error
	GetAccount(email string) (*Account, error)
	GetProfile(accountID int64) (*Profile, error)
}

type DB struct {
	*sql.DB
}

type MockDB struct{}

func NewDB(databaseType string, dataSourceName string) (*DB, error) {
	db, err := sql.Open(databaseType, dataSourceName)
	if err != nil {
		return nil, err
	}
	if err = db.Ping(); err != nil {
		return nil, err
	}
	return &DB{db}, nil
}

func logDatabaseErrors(errorMessage error, message string) {
	log.WithFields(log.Fields{
		"Log":   "database",
		"Error": errorMessage,
	}).Error(message)
}
