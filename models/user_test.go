package models

import "testing"

func TestAccountStructValidation(t *testing.T) {
	u := new(Account)
	u.Profile = new(Profile)

	errors := ValidateFormEntries(u, "", "")

	if len(errors) <= 0 {
		t.Errorf("Expecting errors on struct %v", u)
	}

	u.SetHashedPassword("")

	if len(u.Password) > 0 {
		t.Errorf("Expecting an empty password []byte, but got %v", u.Password)
	}
}
