package models

import (
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
	"testing"
)

func TestDB_CreateAccount(t *testing.T) {
	db, mock, err := sqlmock.New()
	a := new(Account)

	a.Email = "Test@gmail.com"
	a.Password = []byte("abc")
	a.IsAdmin = false

	if err != nil {
		t.Fatalf("Error creating a stub db connection: %s", err)
	}
	defer db.Close()

	mock.ExpectPrepare("INSERT INTO Account").ExpectExec().WithArgs(a.Email, a.Password, a.IsAdmin).
		WillReturnResult(sqlmock.NewResult(1, 1))

	test_db := &DB{db}

	err, _ = test_db.CreateAccount(a)

	if err != nil {
		t.Errorf("error was not expected while updating stats: %s", err)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}

func TestDB_CreateProfile(t *testing.T) {
	db, mock, err := sqlmock.New()
	p := new(Profile)

	p.GivenName = "Kelvin"
	p.Surname = "Wong"
	p.Team = "Budo"
	p.DateOfBirth = "1986-12-21"
	p.Instructor = "Scott"

	if err != nil {
		t.Fatalf("Error creating a stub db connection: %s", err)
	}

	defer db.Close()

	mock.ExpectPrepare("INSERT INTO Profile").ExpectExec().
		WithArgs(int64(1), p.GivenName, p.Surname, p.Team, p.Instructor, p.DateOfBirth).
		WillReturnResult(sqlmock.NewResult(1, 1))

	test_db := &DB{db}
	err = test_db.CreateProfile(p, int64(1))

	if err != nil {
		t.Errorf("error was not expected while updating stats: %s", err)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}