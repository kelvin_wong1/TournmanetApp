package main

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/schema"
	log "github.com/sirupsen/logrus"
	"html/template"
	"net/http"
	"os"
)

type Configuration struct {
	Database struct {
		Host     string `json:"host"`
		Port     string `json:"port"`
		Schema   string `json:"schema"`
		User     string `json:"user"`
		Password string `json:"password"`
		Type     string `json:"databaseType"`
	} `json:"database"`

	Application struct {
		Address      string `json:"address"`
		ReadTimeout  int64  `json:"readTimeout"`
		WriteTimeout int64  `json:"writeTimeout"`
		Static       string `json:"static"`
		Uploads      string `json:"uploads"`
		Session      string `json:"session"`
		MaxAge       int    `json:"maxAge"`
	} `json:"app"`

	Redis struct {
		Address string `json:"address"`
		Port    string `json:"port"`
	} `json:"redis"`
}

var config Configuration

func init() {
	// log file
	log.SetFormatter(&log.JSONFormatter{})
	log.SetLevel(log.InfoLevel)
	loadConfig()
}

func (c Configuration) GetDatabaseConnectionString() (string, string) {
	connectionString := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", c.Database.User, c.Database.Password, c.Database.Host, c.Database.Port,
		c.Database.Schema)

	return c.Database.Type, connectionString
}

func (c Configuration) GetRedisConnectionString() string {
	connectionString := fmt.Sprintf("%s:%s", c.Redis.Address, c.Redis.Port)

	return connectionString
}

// load JSON file for app configuration
func loadConfig() {
	file, err := os.Open("conf/app.json")
	if err != nil {
		log.WithFields(log.Fields{
			"Log":   "Startup",
			"Error": err,
		}).Fatal("Error starting up. Error reading configuration file.")
	}
	decoder := json.NewDecoder(file)
	config = Configuration{}
	err = decoder.Decode(&config)
	if err != nil {
		log.WithFields(log.Fields{
			"Log":   "Startup",
			"Error": err,
		}).Fatal("Error starting up. Could not decode configuration from app.json  ")
	}
}

func generateHTML(writer http.ResponseWriter, data interface{}, htmlPages ...string) {
	// declare an array of string named files
	var files []string

	for _, file := range htmlPages {
		files = append(files, fmt.Sprintf("views/%s.html", file))

	}

	templates := template.Must(template.ParseFiles(files...))
	templates.ExecuteTemplate(writer, "layout", data)
}

func createNewDecoder() {
	decode = schema.NewDecoder()
	decode.IgnoreUnknownKeys(true)
}
