package main

import (
	"crypto/rand"
	"encoding/json"
	"fmt"
	"github.com/kwong21/ryuji-ng/models"
	log "github.com/sirupsen/logrus"
	"io"
	"net/http"
)

func authenticateUser(email string, password string) (bool, *models.Account) {

	a, err := env.db.GetAccount(email)

	if a.Id == int64(-1) || !a.CheckHashedPassword(password) || err != nil {

		log.WithFields(log.Fields{
			"Log":              "user login",
			"Attempt for user": email,
		}).Error("User login failed")

		return false, nil
	}

	if a.CheckHashedPassword(password) {
		a.Password = nil
		a.Profile, err = env.db.GetProfile(a.Id)

		if err != nil {
			log.WithFields(log.Fields{
				"Log":   "user login",
				"Error": err,
			}).Error(fmt.Sprintf("Error retrieving profile for %s", a.Email))
			return false, nil
		}

		return true, a
	}

	return false, nil
}

func createSessionForAuthenticatedUser(w http.ResponseWriter, r *http.Request, a *models.Account) error {
	session, err := env.store.Get(r, config.Application.Session)

	uuid, err := newUUID()
	session.Values["session-id"] = uuid

	session.Save(r, w)

	if err != nil {
		log.WithFields(log.Fields{
			"Log":   "session",
			"Error": err,
		}).Error("Error creating session for authentication")
	} else {
		log.WithFields(log.Fields{
			"Log": "sessions",
		}).Info(fmt.Sprintf("Created new session with id %v", uuid))
	}

	serializedAccount, err := json.Marshal(a)

	err = env.cacheStore.StoreSession(uuid, serializedAccount)

	return err

}

func expireSession(r *http.Request, w http.ResponseWriter) error {
	session, err := env.store.Get(r, config.Application.Session)

	uuid, err := getSessionUUID(r)

	log.WithFields(log.Fields{
		"Log": "sessions",
	}).Info(fmt.Sprintf("Logging user with UUID out %v", uuid))

	session.Options.MaxAge = -1
	session.Save(r, w)
	return err
}

func getSessionUUID(r *http.Request) (string, error) {

	session, err := env.store.Get(r, config.Application.Session)

	if val, ok := session.Values["session-id"].(string); ok {
		return val, nil
	}

	return "", err

}

// newUUID generates a random UUID according to RFC 4122
func newUUID() (string, error) {
	uuid := make([]byte, 16)
	n, err := io.ReadFull(rand.Reader, uuid)
	if n != len(uuid) || err != nil {
		return "", err
	}
	// variant bits; see section 4.1.1
	uuid[8] = uuid[8]&^0xc0 | 0x80
	// version 4 (pseudo-random); see section 4.1.3
	uuid[6] = uuid[6]&^0xf0 | 0x40
	return fmt.Sprintf("%x-%x-%x-%x-%x", uuid[0:4], uuid[4:6], uuid[6:8], uuid[8:10], uuid[10:]), nil
}

// checkForAuthenticatedSession checks if user is logged in
func checkForAuthenticatedSession(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		sessionID, err := getSessionUUID(r)

		if sessionID == "" {
			log.WithFields(log.Fields{
				"Log":   "requests",
				"Page":  r.URL.String(),
				"Error": err,
			}).Warn("Attempt to view protected page without authorization")
			routeErrorPages(w, http.StatusUnauthorized, "unauthorized")

		} else {
			log.WithFields(log.Fields{
				"Log":            "requests",
				"Method":         r.Method,
				"Page":           r.URL.String(),
				"Client":         r.UserAgent(),
				"Remote Address": r.RemoteAddr,
			}).Info("Page request")
			h.ServeHTTP(w, r)
		}
	})
}
