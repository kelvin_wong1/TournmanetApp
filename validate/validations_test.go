package validate

import (
	"testing"
)

// Test email form validation
func TestEmailValidation (t *testing.T) {
	if !(CheckIfValidEmail("someAddress@something.ca")){
		t.Error("Expecting: someAddress@something.ca to pass validation")
	}
}

// Test alpha numeric form validation
func TestAlphaNumeric (t *testing.T){
	if (CheckIfAlphaNumeric("!@#$%^&*")){
		t.Error("Expecting: '!@#$%^&*' to fail alpha numeric validation")
	}
}

// Test alphabet-only input validation
func TestAlphabet (t *testing.T){
	if !(CheckIfAlphaCharacter("abcABC")){
		t.Error("Expecting: 'abcABC' to pass alphabet validation")
	}

	if (CheckIfAlphaCharacter("abcABC_123")){
		t.Error("Expecting: 'abcABC_123' to fail alphabet validation")
	}
}

// Test validateConfirmation
func TestValidateConfirmation (t *testing.T){
	if !(CheckIfInputIsConfirmed("match", "match")){
		t.Error("Expecting: 'match' and 'match' to pass confirmation validation")
	}

	if (CheckIfInputIsConfirmed("match", "Match")){
		t.Error("Expecting: 'match' and 'Match' to fail confirmation validation")
	}
}

// Test validateDateOfBirth
func TestValidateDateOfBirth (t *testing.T){
	if !(CheckIfValidDateOfBirth("1986-12-01")){
		t.Error("Expecting: '1986-12-01' to pass DOB validation")
	}

	if (CheckIfValidDateOfBirth("19816-12-01")){
		t.Error("Expecting: '19816-12-01' to fail DOB validation")
	}
}
