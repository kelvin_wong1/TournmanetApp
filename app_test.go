package main

import (
	"fmt"
	"github.com/google/go-cmp/cmp"
	"github.com/kwong21/ryuji-ng/models"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"
	"github.com/gorilla/sessions"
	"github.com/gorilla/securecookie"
	"github.com/kwong21/ryuji-ng/cache"
)

func init() {
	createNewDecoder()
	env = &Env{db: models.MockDB{}, cacheStore: cache.MockCache{}}
	constructTestingSession()

}

func TestGetHomePageRequestShouldReturnStatusOk(t *testing.T) {
	r, err := http.NewRequest("GET", "/", nil)

	if err != nil {
		t.Errorf("Error running unit tests for TestGetHomePageRequestShouldReturnStatusOk: %v", err)
	}

	w := httptest.NewRecorder()
	router().ServeHTTP(w, r)

	if w.Code != http.StatusOK {
		t.Errorf("Home page did not return %v", http.StatusOK)
	}
}

func TestGetPublicPageRequestsShouldReturnStatusOk(t *testing.T) {
	files, err := ioutil.ReadDir("views/public")

	if err != nil {
		t.Errorf("Listing of public pages failed: %v", err)
	}

	for _, f := range files {
		filenameAsUrlRoute := fmt.Sprintf("/%s", f.Name()[0:len(f.Name())-len(".html")])
		r, err := http.NewRequest("GET", filenameAsUrlRoute, nil)

		if err != nil {
			t.Errorf("Error running unit tests for TestGetPublicPageRequestsShouldReturnStatusOk: %v", err)
		}

		w := httptest.NewRecorder()
		router().ServeHTTP(w, r)

		if w.Code != http.StatusOK {
			t.Errorf("Public page %s did not return %v", filenameAsUrlRoute, http.StatusOK)
		}

	}
}

func TestGetRequestShouldReturnNotFound(t *testing.T) {

	nonExistentRoutes := [2]string{"/page-that-does-not-exist", "/dir/fakeUrl"}

	for _, route := range nonExistentRoutes {
		r, err := http.NewRequest("GET", route, nil)

		if err != nil {
			t.Errorf("Error running unit tests for TestGetRequestShouldReturnNotFound: %v", err)
		}

		w := httptest.NewRecorder()
		router().ServeHTTP(w, r)

		if w.Code != http.StatusNotFound {
			t.Errorf("Non existant page %s did not return %v. Instead got %v", route, http.StatusNotFound, w.Code)
		}
	}
}

func TestCreatingUserFromFormInput(t *testing.T) {

	formValues := url.Values{}
	formValues.Add("Profile.GivenName", "Roger")
	formValues.Add("Profile.Surname", "Rabbit")
	formValues.Add("Email", "roger.rabbit@gmail.com")
	formValues.Add("EmailConfirm", "roger.rabbit@gmail.com")
	formValues.Add("Password", "password")
	formValues.Add("PasswordConfirm", "password")
	formValues.Add("Profile.Team", "Budo MMA")
	formValues.Add("Profile.Instructor", "Scott")
	formValues.Add("Profile.DateOfBirth", "1986-12-01")

	req, err := http.NewRequest("POST", "/sign-up", strings.NewReader(formValues.Encode()))
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	if err != nil {
		t.Errorf("Error creating test data \n, %v", err)
	}
	req.ParseForm()

	a := new(models.Account)
	err = decode.Decode(a, req.PostForm)
	err = a.SetHashedPassword(req.FormValue("Password"))

	if err != nil {
		t.Errorf("Error creating test data \n, %v", err)
	}

	validUser := constructValidStructForTesting()

	if !a.CheckHashedPassword("password") {
		t.Errorf("Expecting password to match")
	}

	validUser.Password = a.Password

	if !cmp.Equal(a, validUser) {
		t.Errorf("Expecting created struct to match known valid struct")
		fmt.Printf("Schema created struct %v", a)
		fmt.Printf("Valid created struct %v", validUser)
	}

	w := httptest.NewRecorder()
	router().ServeHTTP(w, req)

	if w.Code != http.StatusOK {
		t.Errorf("Expecting %v. Got %v", http.StatusOK, w.Code)
	}
	expireTestingSession(req, w)
}

func TestResponseForInvalidUserCreation(t *testing.T) {

	formValues := url.Values{}
	formValues.Add("Profile.GivenName", "")
	formValues.Add("Profile.Surname", "")
	formValues.Add("Profile.Email", "")
	formValues.Add("EmailConfirm", "")
	formValues.Add("Password", "")
	formValues.Add("PasswordConfirm", "")
	formValues.Add("Profile.Instructor", "")
	formValues.Add("Profile.Team", "")
	formValues.Add("Profile.DateOfBirth", "10-12-1180")

	req, err := http.NewRequest("POST", "/sign-up", strings.NewReader(formValues.Encode()))

	if err != nil {
		t.Errorf("Error creating test data \n, %v", err)
	}

	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	w := httptest.NewRecorder()
	router().ServeHTTP(w, req)

	if w.Code != http.StatusBadRequest {
		t.Errorf("Expecting BadRequest response. Got %v", w.Code)
	}
}

func TestResponseForDBErrorShouldReturnInteralServerError(t *testing.T) {
	formValues := url.Values{}
	formValues.Add("Profile.GivenName", "Server")
	formValues.Add("Profile.Surname", "Error")
	formValues.Add("Email", "roger.rabbit@gmail.com")
	formValues.Add("EmailConfirm", "roger.rabbit@gmail.com")
	formValues.Add("Password", "password")
	formValues.Add("PasswordConfirm", "password")
	formValues.Add("Profile.Team", "Budo MMA")
	formValues.Add("Profile.Instructor", "Scott")
	formValues.Add("Profile.DateOfBirth", "1986-12-01")

	req, err := http.NewRequest("POST", "/sign-up", strings.NewReader(formValues.Encode()))
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	if err != nil {
		t.Errorf("Error creating test data \n, %v", err)
	}
	req.ParseForm()

	a := new(models.Account)
	err = decode.Decode(a, req.PostForm)
	err = a.SetHashedPassword(req.FormValue("Password"))

	if err != nil {
		t.Errorf("Error creating test data \n, %v", err)
	}

	w := httptest.NewRecorder()
	router().ServeHTTP(w, req)

	if w.Code != http.StatusInternalServerError {
		t.Errorf("Expecting %v. Got %v", http.StatusInternalServerError, w.Code)
	}
}

func TestResponseWhenNotAuthenticated(t *testing.T) {
	r, err := http.NewRequest("GET", "/user/overview", nil)

	if err != nil {
		t.Errorf("Error running unit tests for TestGetHomePageRequestShouldReturnStatusOk: %v", err)
	}

	w := httptest.NewRecorder()
	constructTestingSession()
	router().ServeHTTP(w, r)

	if w.Code != http.StatusUnauthorized {
		t.Errorf("Home page did not return %v. Instead got %v", http.StatusUnauthorized, w.Code)
	}

	expireTestingSession(r, w)
}

func constructValidStructForTesting() *models.Account {
	u := new(models.Account)
	p := new(models.Profile)

	p.Team = "Budo MMA"
	p.Instructor = "Scott"
	p.GivenName = "Roger"
	p.Surname = "Rabbit"
	p.DateOfBirth = "1986-12-01"

	u.Profile = p
	u.Email = "roger.rabbit@gmail.com"
	u.SetHashedPassword("password")

	return u
}

func constructTestingSession()  {
	store := sessions.NewCookieStore(securecookie.GenerateRandomKey(16))

	store.Options = &sessions.Options{
		Path:     "/",
		MaxAge:   1,
		HttpOnly: true,
	}

	env.store = store
}

func expireTestingSession(r *http.Request, w http.ResponseWriter) {

	session, _ := env.store.Get(r, config.Application.Session)

	session.Options.MaxAge = -1
	session.Save(r, w)
}